@echo off
REM It took me like an hour to get this to work. #batchthings

echo If your osu! is not installed at C:\Users\%USERNAME%\AppData\Local\osu!\Songs you need to edit this script.

REM If your osu! is not installed in the default location you need to change this path.
SET OSU_PATH="C:\Users\%USERNAME%\AppData\Local\osu!\Songs"
cd %OSU_PATH%
del C:\Users\%USERNAME%\Desktop\osusearch_ignored.txt
for /d %%a in (*) do (
	for /f %%i in ("%%a") do (
		if not "%%i"=="Failed" (
			echo %%i, >> C:\Users\%USERNAME%\Desktop\osusearch_ignored.txt
		)
	)
)
echo.
echo ---------------------------------------------------------
echo A file called "osusearch_ignored.txt" was saved to your desktop, which contains the beatmapset ids that you have.
echo.
echo Copy ALL the contents of this file and paste them to the ignored beatmaps field in your account settings.
echo Don't worry about the formatting, it should all be handled by the website.
echo ---------------------------------------------------------
echo.
pause
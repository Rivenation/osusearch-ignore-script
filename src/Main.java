import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        printSeparator();
        System.out.println("You need to place the .jar and .bat file to your Songs folder!");
        printSeparator();
        String osuDir = System.getProperty("user.dir");
        System.out.println("Using folder " + osuDir + ".\nIf this is correct press ENTER, if not, move the files to the correct location and run again.");
        getConfirmation();
        String[] beatmapDirs = new File(osuDir).list();
        ArrayList<Integer> beatmapIds = new ArrayList<Integer>();
        ArrayList<String> failedFolders = new ArrayList<String>();
        System.out.println("Found a total number of " + beatmapDirs.length + " of folders, will now parse their names for ids.");
        for (String folder : beatmapDirs) {
            System.out.println("Parsing folder " + folder);
            String beatmapId = folder.split(" ")[0];
            try {
                beatmapIds.add(Integer.parseInt(beatmapId));
            } catch (NumberFormatException e) {
                failedFolders.add(folder);
                System.out.println(beatmapId + " is not a number, ignoring..");
            }
        }
        System.out.println("\nFound " + beatmapIds.size() + " valid ids, " + failedFolders.size() + " not valid ones.");
        StringBuilder sb = new StringBuilder();
        for (int id : beatmapIds) {
            sb.append(id + ",");
        }
        String beatmapIdsOutput = sb.toString();
        sb = new StringBuilder();
        for (String folder : failedFolders) {
            sb.append(folder + " \r\n");
        }
        String errorOutput = sb.toString();
        BufferedWriter output = null;
        try {
            File file = new File("osusearch_ignored.txt");
            output = new BufferedWriter(new FileWriter(file));
            output.write(beatmapIdsOutput);
            output.close();
            file = new File("osusearch_errors.txt");
            output = new BufferedWriter(new FileWriter(file));
            output.write(errorOutput);
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("\nThe valid ids found were written to osusearch_ignored.txt in this same folder and" +
                " folders whose names were invalid were written to osusearch_errors.txt.\n");
        printSeparator();
        System.out.println("Copy ALL the text in osusearch_ignored.txt and paste it to the Ignored Beatmaps textfield" +
                " on your osu!search account settings.");
        printSeparator();
    }
    public static void getConfirmation() {
        try {
            System.in.read();
        } catch (Exception e) {

        }
    }
    public static void printSeparator() {
        System.out.println("------------------------------------------------------------");
    }
}
